import { LightningElement, track, api } from 'lwc';
import  chart from '@salesforce/resourceUrl/ChartJs'
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
const emptySharesList = [{ sfAccountName: null, share: 0, email: null, recipientId: null }, { sfAccountName: null, share: 0, email: null, recipientId: null }, { sfAccountName: null, share: 0, email: null, recipientId: null },
{ sfAccountName: null, share: 0, email: null, recipientId: null }, { sfAccountName: null, share: 0, email: null, recipientId: null }];

const bgColors = ["#F9A45E", "#81D389", "#FFD847", "#FD4D34", "#ACE0F4", "#995DCF", "#8AD7DE", "#4887BE", "#F4A2C3", "#B4D568",
                "#DFEEBD", "#D76190", "#76B0E3", "#5BB9C1", "#CEA1F6", "#6EBEDD", "#F47A69", "#FFBF47", "#B0DAB4", "#FF7C10"];

export default class LwcChart extends LightningElement {

    @track donutChart;
    @track placeHolderChart;

    @track data = {};
    @track displayData = [];
    @track _initialdata;
    @track localColors = JSON.parse(JSON.stringify(bgColors));

    @api
    get initialdata() {
        return this._initialdata;
    }
    set initialdata(data) {
        if (data) {
            this._initialdata = JSON.parse(JSON.stringify(data));
        }
    }

    @track localColors = JSON.parse(JSON.stringify(bgColors));

    @track placeholderData = {
        datasets: [{
            data: [100]
        }],
        backgroundColor: ['F2EEE8']

    };

    initializeData(sharesList) {
    
        var datavaluelist = [];
        var bgcolorlist = [];
        var labellist = [];
        this.displayData = [];
        try {
            if (!sharesList || sharesList.length === 0) {
                this.hideTable = true;
            } else {
                sharesList.forEach((displayData, index) => {

                    datavaluelist.push(displayData.share);
                    bgcolorlist.push(this.localColors[index]);
                    displayData.dataStyle = 'background-color:' + this.localColors[index];
                    displayData.id = this.localColors[index];
                    displayData.color = this.localColors[index];
                    displayData.bgColor = this.localColors[index];
                    displayData.sfAccountName = displayData.sfAccountName ? displayData.sfAccountName : displayData.accountName;
                    labellist.push(displayData.sfAccountName);
                    this.displayData.push(displayData);
                });
            }

            this.colorPalletteCount = sharesList.length;
            this.data = {
                datasets:
                    [{
                        data: datavaluelist,
                        backgroundColor: bgcolorlist,
                        hoverBackgroundColor: bgcolorlist
                    }],
                labels: labellist
            };

            this.initialEditData = JSON.parse(JSON.stringify(this.displayData));

        } catch (e) {
            console.error('Error ' + JSON.stringify(e));
        }
    }

    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
    }

    addValue() {
        var dataVal = 0;
        var color = this.localColors[0];
        if (this.displayData.length < 20) {
            color = this.localColors[this.displayData.length];
        } else  {
            color = '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
        }
        this.donutChart.data.labels.push('');
        this.donutChart.data.datasets.forEach((dataset) => {
            dataset.data.push(dataVal);
            dataset.backgroundColor.push(color);
        });
        this.donutChart.update();

        this.displayData.push({
            sfAccountName: null,
            share: 0,
            id: color,
            bgColor: color,
            email: null,
            dataStyle: 'background-color:' + color
        });
     }

    connectedCallback() {
        Promise.all([
            loadScript(this, chart + '/dist/Chart.bundle.min.js'),
            loadStyle(this, chart + '/dist/Chart.min.css')
        ]).then(() => {
        	let sharesList = [];
            //this.colorPalletteCount = this.initialdata.length;
            if (!this.initialdata || this.initialdata.length === 0) {
            	sharesList = JSON.parse(JSON.stringify(emptySharesList));
	        } else {
	            sharesList = JSON.parse(JSON.stringify(this.initialdata));
	        }

	        this.initializeData(sharesList);
	        this.loadChart();
        }).catch(error => {
            console.error('Error On Scripts load ' + JSON.stringify(error));
        });
    }

    loadChart() {
        var ctx = this.template.querySelector(".donut");
        var placeHolder = this.template.querySelector(".placeholder-chart");
        if (this.donutChart) {
            this.donutChart.destroy();
        }
        this.donutChart = new Chart(ctx, {
            type: 'doughnut',
            data: this.data,
            options: {
                legend: { display: false }
            }
        });

        this.placeHolderChart = new Chart(placeHolder, {
            type: 'doughnut',
            data: this.placeholderData,
            options: {
                legend: { display: false }
            }
        });
    }

    handleChange(e) {
       var updatedValue = e.currentTarget.value;
        //var color = e.currentTarget.dataset.key;
        var chartfield = e.currentTarget.dataset.chartfield;
        var displayField = e.currentTarget.dataset.field;
        let index =  Number(e.currentTarget.dataset.key);
        if (chartfield && chartfield === 'data') {
            this.donutChart.data.datasets[0].data[index] = updatedValue;
        } else if (chartfield && chartfield === 'label') {
            this.donutChart.data.labels[index] = updatedValue;
        }

        this.displayData[index][displayField] = updatedValue;
        this.donutChart.update();
     //   this.template.querySelector('c-show-toast').hideToast();   
    }

    handleDelete(e) {
        const index = Number(e.currentTarget.dataset.key);
        this.displayData.splice(index, 1);
        this.donutChart.data.labels.splice(index, 1);
        this.donutChart.data.datasets.forEach((dataset) => {
            dataset.data.splice(index, 1);
            dataset.backgroundColor.splice(index, 1);
        });
        this.donutChart.update();
        let splicedColor = this.localColors.splice(index, 1);
        this.localColors.push(splicedColor);
        //this.template.querySelector('c-show-toast').hideToast();
    }

    handleCancel() {
        this.donutChart.clear();
        this.donutChart.destroy();
        this.initializeData(JSON.parse(JSON.stringify(this.initialdata)));
        this.loadChart();
        this.disableEdit();
        this.localColors = JSON.parse(JSON.stringify(bgColors));
    }

    handleSave() {
    	//save your implementation
    }



}